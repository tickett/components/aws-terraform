# AWS Terraform

AWS Terraform will deploy Terraform configuration to AWS, using OIDC to authenticate and storing the state using the GitLab backend.

## Usage

```yml
include:
  - component: gitlab.com/tickett/components/aws-terraform/aws-terraform@main
    inputs:
      tf_root: ${CI_PROJECT_DIR}/terraform
    rules:
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

## Inputs

### `image`

The GitLab Terraform Docker image.

### `tf_root`

The path to the terraform config.

### `tf_state_name`

The Terraform state name to use in GitLab.

## Variables

An AWS IAM role should be configured with a trust relationship looking something like:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Federated": "arn:aws:iam::<ACCOUNT_ID>:oidc-provider/gitlab.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringEquals": {
                    "gitlab.com:aud": "https://gitlab.com"
                },
                "StringLike": {
                    "gitlab.com:sub": "project_path:group-path/project-path:*"
                }
            }
        }
    ]
}
```

### `AWS_ROLE_ARN`

The ARN for the AWS role created above.

### `AWS_WEB_IDENTITY_TOKEN_FILE`

This should be a CI file variable containing `${OIDC_TOKEN}`.
